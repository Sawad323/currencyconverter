//
//  ViewController.swift
//  USDtoYen
//
//  Created by Saker Awad on 1/14/18.
//  Copyright © 2018 Saker Awad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var myLabel1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func myButton1(_ sender: Any) {
        let textFieldValue = Double(textField1.text!)
        
        if textFieldValue != nil{
            let result = Double (textFieldValue! * 112.57)
            
            myLabel1.text = "$\(textFieldValue!) = ¥\(result)"
            textField1.text = ""
        } else {
            myLabel1.text = "This field cannot be blanl!"
        }
    }
    
}
